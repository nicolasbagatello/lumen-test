<?php

/**
 * File created to hold Config keys common to all developers avoid sensitive data / custom data
 * that should be added in .env file
 *
 * User: nicolas
 */

return [
    'default_graph_version' => 'v3.0',
    'default_cache_expiry_time' => 3600
];
