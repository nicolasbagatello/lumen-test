<?php

use App\Http\Controllers\FbUserInfo;
use Facebook\FacebookResponse;
use App\Helpers\RedisWrapper;

class FbUserInfoTest extends TestCase
{

    /**
     *
     * @var FbUserInfo
     */
    private $object;

    /**
     *
     * @var Facebook\Facebook|PHPUnit_Framework_MockObject_MockObject
     */
    private $mockFacebook;

    /**
     *
     * @var FacebookResponse|PHPUnit_Framework_MockObject_MockObject
     */
    private $mockFacebookResponse;

    /**
     *
     * @var RedisWrapper|PHPUnit_Framework_MockObject_MockObject
     */
    private $mockRedisWrapper;

    public function setUp()
    {
        $this->mockFacebook = $this->getMockBuilder(Facebook\Facebook::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockFacebookResponse = $this->getMockBuilder(FacebookResponse::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockRedisWrapper = $this->getMockBuilder(RedisWrapper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->object = new FbUserInfo;

        $reflection = new ReflectionClass($this->object);
        $reflection_property = $reflection->getProperty('fb');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->object, $this->mockFacebook);

        $reflection_property = $reflection->getProperty('fbResponse');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->object, $this->mockFacebookResponse);

        $reflection_property = $reflection->getProperty('redis');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->object, $this->mockRedisWrapper);
    }

    public function tearDown()
    {
        unset($this->mockFacebook);
        unset($this->mockFacebookResponse);
        unset($this->mockRedisWrapper);
    }

    /**
     * Test the get cached value for FbUserInfo@ShowUserData
     *
     * @return void
     * @throws Facebook\Exceptions\FacebookSDKException
     */
    public function testShowUserDataCached()
    {
        $this->mockRedisWrapper->expects($this->once())
            ->method('searchLocalCache')
            ->with($this->isType('integer'))
            ->will($this->returnValue('test,1'));

        $this->mockFacebook->expects($this->never())
            ->method('get');

        $this->mockFacebookResponse->expects($this->never())
            ->method('getGraphNode');

        $this->object->showUserData(1);
    }

    /**
     * Test the happy path for FbUserInfo@ShowUserData
     *
     * @return void
     * @throws Facebook\Exceptions\FacebookSDKException
     */
    public function testShowUserDataNonCached()
    {
        $this->mockRedisWrapper->expects($this->once())
            ->method('searchLocalCache')
            ->with($this->isType('integer'))
            ->will($this->returnValue(false));

        $this->mockFacebook->expects($this->once())
                    ->method('get')
                    ->with($this->isType('string'))
                    ->will($this->returnValue($this->mockFacebookResponse));

        $this->mockFacebookResponse->expects($this->once())
           ->method('getGraphNode')
           ->will($this->returnValue(new ArrayObject(['name' => 'test', 'id' => 1])));

        $this->object->showUserData(1);
    }

    /**
     * Test when we get an Exception for FbUserInfo@ShowUserData
     *
     * @return void
     */
    public function testShowUserDataFbException()
    {
        $this->mockRedisWrapper->expects($this->once())
            ->method('searchLocalCache')
            ->with($this->isType('integer'))
            ->will($this->returnValue(false));

        $this->mockFacebook->expects($this->once())
            ->method('get')
            ->will($this->throwException(new Exception('TestException', 2020202)));
        try {
            $this->object->showUserData(1);
        } catch (Exception $e) {
            $this->assertInstanceOf('Exception', $e);
            $this->assertEquals('TestException', $e->getMessage());
        }
    }
}
