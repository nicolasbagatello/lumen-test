# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).

## How to install / run this project in your local (Linux Only  ¯\_(ツ)_/¯ )

- clone the repo (git clone https://nicolasbagatello@bitbucket.org/nicolasbagatello/lumen-test.git) 
- Install composer in your local (https://getcomposer.org/doc/00-intro.md)
- Install redis in your local (https://redis.io/topics/quickstart)
- Run composer update
- Run the PHP server (php -S localhost:8000 -t public)
- Run the Redis server (/etc/init.d/redis-server start)
- Optional you can make sure Redis is running (redis-cli ping)
- Rename (.env.example) file to .env open it and fill the missing credentials with your data 

## Dev Notes (already implemented):

- I've set all my credentials and sensitive data in .env file
- Common configuration data it's store in config/appCustomConfig.php
- API Request parameters are sanitized
- Catch FB SDK / API exceptions to avoid exposing information if something goes wrong
- Logging with monolog in  storage/logs/lumen.log
- Added Redis cache
- PHPUnit tests added for the controller
- The whole project is psr-2 compliant. More info (https://www.php-fig.org/psr/psr-2/)
- Im using this component to fix my coding style (https://github.com/FriendsOfPHP/PHP-CS-Fixer)
        | Example php ./vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix ./ --rules=@PSR2

## Nice to have (maybe in the future)

- Secured some of these API endpoints with JSON Web Tokens. (https://jwt.io/)
- Dockerize it! (https://docs.docker.com/get-started/)
- Decoupling your application from the framework (http://thesolidphp.com/decoupling-application-from-framework/)


##### Endpoint Example
        
curl --request GET \
  --url http://localhost:8000/v1/facebook/profile/{user-id} \
  --header 'authorization: Bearer {Insert-valid-token-here}'