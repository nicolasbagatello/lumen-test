<?php

// TODO: remove this router that output the framework version
$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'v1'], function () use ($router) {
    // we make sure the request parameter is an integer for security reasons
    $router->get('facebook/profile/{id:[0-9]+}', ['uses' => 'FbUserInfo@showUserData']);

    // TODO: remove or edit the following examples
    //$router->post('authors', ['uses' => 'AuthorController@create']);
    //$router->delete('authors/{id}', ['uses' => 'AuthorController@delete']);
    //$router->put('authors/{id}', ['uses' => 'AuthorController@update']);
});
