<?php

namespace App\Http\Controllers;

use Facebook;
use Log;
use Facebook\FacebookResponse;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;
use App\Helpers\RedisWrapper;

class FbUserInfo extends Controller
{
    /**
     *  @var Facebook\Facebook
     */
    protected $fb;

    /**
     *  @var FacebookResponse
     */
    protected $fbResponse;

    /**
     *  @var RedisWrapper
     */
    protected $redis;

    /**
     * Create a new FbUserInfo controller instance.
     *
     */

    public function __construct()
    {
        // get sensitive app data from .env file :)
        $this->fb = new Facebook\Facebook([
            'app_id' => env('APP_FB_ID'),
            'app_secret' => env('APP_FB_SECRET'),
            'default_graph_version' => config('appCustomConfig.default_graph_version'),
            'default_access_token' => env('APP_FB_DEFAULT_ACCESS_TOKEN'),
        ]);

        $this->redis = new RedisWrapper();
    }

    /**
     * search for user data and set up the json response
     *
     * @param $id integer
     * @return string JSON
     * @throws Facebook\Exceptions\FacebookSDKException
     */
    public function showUserData($id)
    {
        $cachedValue = $this->redis->searchLocalCache($id);
        if ($cachedValue) {
            $value = explode(',', $cachedValue); // grab the stored value and change it to array
            return response()->json(['name' => $value[0], 'id' => $value[1]]);
        } else {
            try {
                // Returns a `FacebookFacebookResponse` object
                $this->fbResponse = $this->fb->get(
                    '/' . $id  //TODO: remove -> users ID examples 100025797858188, 100025811175672, 100025743924210, 100025830733960, 10216645906126156 my ID
                );
            } catch (FacebookResponseException $e) {
                Log::error($e->getMessage()); // Log the error
                abort(404); // Catch the FB exception and trow a 404 so the client hitting the public API endpoint does not know what are we doing under the hood
                exit;
            } catch (FacebookSDKException $e) {
                Log::error($e->getMessage()); // Log the error
                abort(500); // Catch the FB exception and trow a 500 so the client hitting the public API endpoint does not know what are we doing under the hood
                exit;
            }
            $graphNode = $this->fbResponse->getGraphNode();
            $response = response()->json($graphNode->getIterator());
            $userName =  $graphNode->getIterator()->offsetGet('name');
            $userId = $graphNode->getIterator()->offsetGet('id');
            $this->redis->saveLocalCache($id, $userName.','.$userId, config('appCustomConfig.default_cache_expiry_time')); // Store in redis as string for 1 hour

            return $response;
        }
    }
}
