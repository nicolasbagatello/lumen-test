<?php

namespace App\Helpers;

use Cache;

class RedisWrapper
{
    /**
     * search for user data in our local redis cache
     *
     * @param $id integer
     * @return string | boolean
     */
    public function searchLocalCache($id)
    {
        if ($this->checkRedisCacheIsUp()) {
            $localKey = app('redis')->get($id);
            if ($localKey) {
                return $localKey;
            } else {
                return false;
            }
        }
    }

    /**
     * save data in our local redis cache. Default value for expiring the keys is 1 day in seconds
     *
     * @param $id integer
     * @param $info string
     * @param $time integer
     *
     * @return void
     */
    public function saveLocalCache($id, $info, $time = 86400)
    {
        if ($this->checkRedisCacheIsUp()) {
            app('redis')->setex($id, $time, $info);
        }
    }

    /**
     * check if redis cache is up and running
     *
     * @return boolean
     */
    protected function checkRedisCacheIsUp()
    {
        try {
            app('redis')->ping();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
